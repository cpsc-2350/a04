import { covidDataSource, UpstreamCovidEntry } from './covidDataSource';

import sampleData from './covidDataSource.spec.in.json';
import expected from './covidDataSource.spec.expected.json';

/* eslint-disable @typescript-eslint/unbound-method */

/* TODO

  MUT: covidDataSource

  mock axios

  test that covidDataSource() calls the expected URL

  test that when the input value is `sampleData`, the result is `expected`

*/

//Destructuring object keys
const unwrap = ({
  HA_Pop20,
  Recovered,
  Deaths,
  Cases,
  Date_Updat,
  Hospitalized,
  CurrentlyHosp,
  EverICU,
  CurrentlyICU,
  NewCases,
  LabCases,
  EpiCases,
  ActiveCases,
}: UpstreamCovidEntry) => ({
  HA_Pop20,
  Recovered,
  Deaths,
  Cases,
  Date_Updat,
  Hospitalized,
  CurrentlyHosp,
  EverICU,
  CurrentlyICU,
  NewCases,
  LabCases,
  EpiCases,
  ActiveCases,
});
const sampleFiltered = sampleData.features.map(a => unwrap(a.attributes));
const expectedFiltered = expected.map(a => unwrap(a));

// mocking the axios dependency
import axios from 'axios';
jest.mock('axios');
const axiosMocked = axios as jest.Mocked<typeof axios>;

describe('covidDataSource', () => {
  describe('BC Health Authorities report', () => {
    test('COVID data api', async () => {
      //test that covidDataSource() calls the expected URL
      expect.assertions(3);
      await expect(covidDataSource()).rejects.toThrow();
      expect(axiosMocked.get).toHaveBeenCalledTimes(1);
      expect(axiosMocked.get).toHaveBeenCalledWith(
        'https://services1.arcgis.com/xeMpV7tU1t4KD3Ei/arcgis/rest/services/COVID19_Cases_by_BC_Health_Authority/FeatureServer/0/query?f=json&where=FID%20NOT%20IN(6)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&resultOffset=0&resultRecordCount=50&resultType=standard&cacheHint=true'
      );
    });
  });
  describe('input vs expected', () => {
    test('testing input', async () => {
      expect.assertions(1);
      axiosMocked.get.mockResolvedValueOnce({
        data: {
          features: [{ attributes: sampleFiltered }],
        },
      });
      const actual: UpstreamCovidEntry[] = await covidDataSource();
      expect(JSON.stringify(actual)).toBe(JSON.stringify([expectedFiltered]));
    });
  });
});
