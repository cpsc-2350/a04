import axios from 'axios';

export interface UpstreamCovidEntry {
  // FID: number;
  // HA_ID: string;
  // HA_Name: string;
  HA_Pop20: number;
  Recovered: number;
  Deaths: number;
  Cases: number;
  Date_Updat: number;
  Hospitalized: number;
  CurrentlyHosp: number;
  EverICU: number;
  CurrentlyICU: number;
  NewCases: number;
  LabCases: number;
  EpiCases: number;
  ActiveCases: number;
}

export type covidDataSourceType = () => Promise<UpstreamCovidEntry[]>;

interface UpstreamCovidData {
  features: { attributes: UpstreamCovidEntry }[];
}

const url =
  'https://services1.arcgis.com/xeMpV7tU1t4KD3Ei/arcgis/rest/services/COVID19_Cases_by_BC_Health_Authority/FeatureServer/0/query?f=json&where=FID%20NOT%20IN(6)&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&resultOffset=0&resultRecordCount=50&resultType=standard&cacheHint=true';

export async function covidDataSource(): Promise<UpstreamCovidEntry[]> {
  const resp = await axios.get(url);
  const data = resp.data as UpstreamCovidData;
  return data.features.map(a => a.attributes);
}
