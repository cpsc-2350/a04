import { UpstreamCovidEntry, covidDataSourceType } from './covidDataSource';

export type CovidTotals = Omit<UpstreamCovidEntry, 'Date_Updat'> & {
  Date_Updat: Date;
};

export interface CovidTotaler {
  getCovidTotals(): Promise<CovidTotals>;
}

function reducer(
  acc: UpstreamCovidEntry,
  cur: UpstreamCovidEntry
): UpstreamCovidEntry {
  for (const i in acc) {
    const ii = i as keyof UpstreamCovidEntry;
    if (ii != 'Date_Updat') {
      acc[ii] += cur[ii];
    } else {
      acc[ii] = Math.max(acc[ii], cur[ii]);
    }
  }
  return acc;
}
export const emptyTotals = Object.freeze({
  HA_Pop20: 0,
  Recovered: 0,
  Deaths: 0,
  Cases: 0,
  Date_Updat: 0,
  Hospitalized: 0,
  CurrentlyHosp: 0,
  EverICU: 0,
  CurrentlyICU: 0,
  NewCases: 0,
  LabCases: 0,
  EpiCases: 0,
  ActiveCases: 0,
});

export class CovidTotalerImpl implements CovidTotaler {
  constructor(private readonly covidDataSource: covidDataSourceType) {}

  async getCovidTotals(): Promise<CovidTotals> {
    const upstream = await this.covidDataSource();
    const acc = upstream.reduce(reducer, Object.assign({}, emptyTotals));
    return Object.assign(acc, { Date_Updat: new Date(acc.Date_Updat) });
  }
}
