import { CovidTotalerImpl } from './CovidTotals';
import { covidDataSource } from './covidDataSource';

new CovidTotalerImpl(covidDataSource)
  .getCovidTotals()
  .then(console.log)
  .catch(console.error);
