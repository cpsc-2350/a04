import {
  CovidTotaler,
  CovidTotalerImpl,
  emptyTotals,
  CovidTotals,
} from './CovidTotals';
import {
  covidDataSourceType,
  covidDataSource,
  UpstreamCovidEntry,
} from './covidDataSource';

import sample from './CovidTotals.spec.sum.in.json';
import expected from './CovidTotals.spec.sum.expected.json';

/* TODO

  MUT: CovidTotalerImpl

  mock the covidDataSource function with:
  jest.fn() as jest.MockedFunction<covidDataSourceType>

  test that the module under test (MUT) calls the mocked function

  test summing values:
  when `sample` is provided, that the result is `expected`
  use JSON.stringify(object) to make comparing easier

  test that the most recent date is used [2 test cases]
  use: Object.assign({}, emptyTotals, {
          Date_Updat: new Date(string).getTime(),
        })
  to assign dates for testing

*/

//Destructuring object keys
const unwrap = ({
  HA_Pop20,
  Recovered,
  Deaths,
  Cases,
  Date_Updat,
  Hospitalized,
  CurrentlyHosp,
  EverICU,
  CurrentlyICU,
  NewCases,
  LabCases,
  EpiCases,
  ActiveCases,
}: UpstreamCovidEntry) => ({
  HA_Pop20,
  Recovered,
  Deaths,
  Cases,
  Date_Updat,
  Hospitalized,
  CurrentlyHosp,
  EverICU,
  CurrentlyICU,
  NewCases,
  LabCases,
  EpiCases,
  ActiveCases,
});
const sampleFiltered = sample.map(a => unwrap(a));

describe('CovidTotals', () => {
  let mut: CovidTotaler;
  let mock: jest.MockedFunction<() => Promise<UpstreamCovidEntry[]>>;
  /*
  (1) signature of the function mocked (arguments, returning types).
  (2) in this case, is the signature of covidDataSource()
  (3) this is the mock that we need to create a mock implementation to be called when getCovidTotals() is called,
  because we instantiate the object "mut" giving "mock" as argument. The method getCovidTotals() calls covidDAtaSource()
  inside its code. For this reason, we need to mock this covidDataSource() response, and the way we do this is passing
  the created "mock" variable as argumento to the instantiated CovidTotalerImpl object called "mut". 
  */

  const mockcovidDataSource = jest.fn(covidDataSource) as jest.MockedFunction<
    covidDataSourceType
  >;
  /*
  Here, the function alone is being mocked to test a call from this MUT
  */

  beforeEach(() => {
    mock = jest.fn();
    mut = new CovidTotalerImpl(mock);
  });

  describe('testing function call', () => {
    test('calling covidDataSourceType', async () => {
      expect.assertions(1);
      await mockcovidDataSource();
      expect(mockcovidDataSource).toHaveBeenCalledTimes(1);
    });
  });

  describe('testing input vs expected', () => {
    test('testing input', async () => {
      expect.assertions(1);
      mock.mockImplementation(
        (): Promise<UpstreamCovidEntry[]> => {
          return Promise.resolve(sampleFiltered);
        }
      );
      const actual: CovidTotals = await mut.getCovidTotals();
      expect(JSON.stringify(actual)).toBe(JSON.stringify(expected));
    });
  });
});
