# TESTING and CI

## Background Info

* The provided code fetches COVID data reported by the five BC Health Authorities, and sums the data.
* The `Date_Updat` field on the input data is the /epoch time/ in milliseconds (time since Janyary 1st, 1970).  Instead of summing these fields, the most recent time is taken and converted into a javascript Date object.

* `covidDataSource` fetches the data.
* `CovidTotalerImpl` sums the data.

## Requierments

* Provide the necessary project files and complete the provided `covidDataSource.spec.ts` and `CovidTotals.spec.ts` files.
* Do NOT alter the provided `covidDataSource.ts`, `CovidTotals.ts`, or `.json` files.
* Mock dependencies as indicated. Do NOT depend on any live data as it will change over time.
* The `*.json` files provide inputs and matching expected outputs for two tests, but you will have to make your own inputs for at least one test. You are NOT required to create any new `.json` files.
* The `index.ts` is provided for reference only. It does NOT need to be modified.
* Executing `npm run build` should compile and lint the code, and run the tests.
* All code should compile and lint cleanly.  You should NOT need to disable any linting rules except for `@typescript-eslint/unbound-method`.
* All code should be formatted according to the `prettier` rules used in the course.
* Code should be compiled, and tests should be run whenever a commit is made to gitlab.
* Follow best practices as demonstrated in the course to receive full marks.

## Out of Scope

* You do NOT need to create a docker container for this application.
* You do NOT need to deploy the application to any environments.
* You do NOT need to provide any entry or end points for the application, except for the `.spec.ts` files indicated above.

## Submission

* Submit ALL project files, `.ts` files, and `.json` files.
* Do NOT submit the `build`, `coverage`, or `node_modules` directory.
